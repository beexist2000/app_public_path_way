import 'package:app_public_path_way/config/config_api.dart';
import 'package:app_public_path_way/model/path_info_list.dart';
import 'package:app_public_path_way/model/path_info_request.dart';
import 'package:app_public_path_way/model/path_info_response.dart';
import 'package:dio/dio.dart';

class RepoPathInfo {
  Future<PathInfoList> getList(PathInfoRequest request) async {
    var resultJson = {
      "comMsgHeader": {
        "errMsg": null,
        "responseMsgID": null,
        "responseTime": null,
        "requestMsgID": null,
        "returnCode": null,
        "successYN": null
      },
      "msgHeader": {
        "headerMsg": "",
        "headerCd": "0",
        "itemCount": 0
      },
      "msgBody": {
        "itemList": [
          {
            "time": "55",
            "distance": "20743",
            "pathList": [
              {
                "routeNm": "6003",
                "routeId": "100100414",
                "fname": "대일고등학교",
                "fid": "115000542",
                "fx": "126.86346080737559",
                "fy": "37.53895951063778",
                "tname": "대림역",
                "tid": "116000406",
                "tx": "126.8943952894773",
                "ty": "37.493296453557356",
                "railLinkList": null
              },
              {
                "routeNm": "2호선",
                "routeId": null,
                "fname": "대림역",
                "fid": "02330",
                "fx": "126.89489286542995",
                "fy": "37.49331491933711",
                "tname": "강남역",
                "tid": "02220",
                "tx": "127.0279353277511",
                "ty": "37.4980170747733",
                "railLinkList": [
                  {
                    "railLinkId": "1"
                  },
                  {
                    "railLinkId": "2"
                  },
                  {
                    "railLinkId": "3"
                  },
                  {
                    "railLinkId": "4"
                  },
                  {
                    "railLinkId": "5"
                  },
                  {
                    "railLinkId": "6"
                  },
                  {
                    "railLinkId": "7"
                  },
                  {
                    "railLinkId": "8"
                  },
                  {
                    "railLinkId": "9"
                  },
                  {
                    "railLinkId": "10"
                  },
                  {
                    "railLinkId": "11"
                  }
                ]
              }
            ]
          },
          {
            "time": "58",
            "distance": "19032",
            "pathList": [
              {
                "routeNm": "6716",
                "routeId": "100100451",
                "fname": "목동시장.대일고교.배광교회",
                "fid": "114000013",
                "fx": "126.86382293571833",
                "fy": "37.538950917807185",
                "tname": "염창역.서울도시가스",
                "tid": "115000002",
                "tx": "126.87498987442348",
                "ty": "37.546910929457056",
                "railLinkList": null
              },
              {
                "routeNm": "9호선",
                "routeId": null,
                "fname": "염창역",
                "fid": "41100",
                "fx": "126.87485402276236",
                "fy": "37.54693781841077",
                "tname": "신논현역",
                "tid": "41250",
                "tx": "127.02547207054795",
                "ty": "37.504757877769535",
                "railLinkList": [
                  {
                    "railLinkId": "1"
                  },
                  {
                    "railLinkId": "2"
                  },
                  {
                    "railLinkId": "3"
                  },
                  {
                    "railLinkId": "4"
                  },
                  {
                    "railLinkId": "5"
                  },
                  {
                    "railLinkId": "6"
                  },
                  {
                    "railLinkId": "7"
                  },
                  {
                    "railLinkId": "8"
                  },
                  {
                    "railLinkId": "9"
                  },
                  {
                    "railLinkId": "10"
                  },
                  {
                    "railLinkId": "11"
                  },
                  {
                    "railLinkId": "12"
                  },
                  {
                    "railLinkId": "13"
                  },
                  {
                    "railLinkId": "14"
                  },
                  {
                    "railLinkId": "15"
                  }
                ]
              },
              {
                "routeNm": "6004광주",
                "routeId": "234000314",
                "fname": "신논현역.영신빌딩",
                "fid": "121000941",
                "fx": "127.02514323951621",
                "fy": "37.50232497252528",
                "tname": "신분당선강남역",
                "tid": "121000009",
                "tx": "127.02839826372518",
                "ty": "37.49598948243004",
                "railLinkList": null
              }
            ]
          },
          {
            "time": "58",
            "distance": "19032",
            "pathList": [
              {
                "routeNm": "602",
                "routeId": "100100087",
                "fname": "목동시장.대일고교.배광교회",
                "fid": "114000013",
                "fx": "126.86382293571833",
                "fy": "37.538950917807185",
                "tname": "염창역.서울도시가스",
                "tid": "115000002",
                "tx": "126.87498987442348",
                "ty": "37.546910929457056",
                "railLinkList": null
              },
              {
                "routeNm": "9호선",
                "routeId": null,
                "fname": "염창역",
                "fid": "41100",
                "fx": "126.87485402276236",
                "fy": "37.54693781841077",
                "tname": "신논현역",
                "tid": "41250",
                "tx": "127.02547207054795",
                "ty": "37.504757877769535",
                "railLinkList": [
                  {
                    "railLinkId": "1"
                  },
                  {
                    "railLinkId": "2"
                  },
                  {
                    "railLinkId": "3"
                  },
                  {
                    "railLinkId": "4"
                  },
                  {
                    "railLinkId": "5"
                  },
                  {
                    "railLinkId": "6"
                  },
                  {
                    "railLinkId": "7"
                  },
                  {
                    "railLinkId": "8"
                  },
                  {
                    "railLinkId": "9"
                  },
                  {
                    "railLinkId": "10"
                  },
                  {
                    "railLinkId": "11"
                  },
                  {
                    "railLinkId": "12"
                  },
                  {
                    "railLinkId": "13"
                  },
                  {
                    "railLinkId": "14"
                  },
                  {
                    "railLinkId": "15"
                  }
                ]
              },
              {
                "routeNm": "8502화성",
                "routeId": "233000333",
                "fname": "신논현역.영신빌딩",
                "fid": "121000941",
                "fx": "127.02514323951621",
                "fy": "37.50232497252528",
                "tname": "신분당선강남역",
                "tid": "121000009",
                "tx": "127.02839826372518",
                "ty": "37.49598948243004",
                "railLinkList": null
              }
            ]
          },
          {
            "time": "58",
            "distance": "19032",
            "pathList": [
              {
                "routeNm": "6714",
                "routeId": "100100331",
                "fname": "목동시장.대일고교.배광교회",
                "fid": "114000013",
                "fx": "126.86382293571833",
                "fy": "37.538950917807185",
                "tname": "염창역.서울도시가스",
                "tid": "115000002",
                "tx": "126.87498987442348",
                "ty": "37.546910929457056",
                "railLinkList": null
              },
              {
                "routeNm": "9호선",
                "routeId": null,
                "fname": "염창역",
                "fid": "41100",
                "fx": "126.87485402276236",
                "fy": "37.54693781841077",
                "tname": "신논현역",
                "tid": "41250",
                "tx": "127.02547207054795",
                "ty": "37.504757877769535",
                "railLinkList": [
                  {
                    "railLinkId": "1"
                  },
                  {
                    "railLinkId": "2"
                  },
                  {
                    "railLinkId": "3"
                  },
                  {
                    "railLinkId": "4"
                  },
                  {
                    "railLinkId": "5"
                  },
                  {
                    "railLinkId": "6"
                  },
                  {
                    "railLinkId": "7"
                  },
                  {
                    "railLinkId": "8"
                  },
                  {
                    "railLinkId": "9"
                  },
                  {
                    "railLinkId": "10"
                  },
                  {
                    "railLinkId": "11"
                  },
                  {
                    "railLinkId": "12"
                  },
                  {
                    "railLinkId": "13"
                  },
                  {
                    "railLinkId": "14"
                  },
                  {
                    "railLinkId": "15"
                  }
                ]
              },
              {
                "routeNm": "M5438광주",
                "routeId": "234001660",
                "fname": "신논현역.영신빌딩",
                "fid": "121000941",
                "fx": "127.02514323951621",
                "fy": "37.50232497252528",
                "tname": "신분당선강남역",
                "tid": "121000009",
                "tx": "127.02839826372518",
                "ty": "37.49598948243004",
                "railLinkList": null
              }
            ]
          },
          {
            "time": "58",
            "distance": "19032",
            "pathList": [
              {
                "routeNm": "602",
                "routeId": "100100087",
                "fname": "목동시장.대일고교.배광교회",
                "fid": "114000013",
                "fx": "126.86382293571833",
                "fy": "37.538950917807185",
                "tname": "염창역.서울도시가스",
                "tid": "115000002",
                "tx": "126.87498987442348",
                "ty": "37.546910929457056",
                "railLinkList": null
              },
              {
                "routeNm": "9호선",
                "routeId": null,
                "fname": "염창역",
                "fid": "41100",
                "fx": "126.87485402276236",
                "fy": "37.54693781841077",
                "tname": "신논현역",
                "tid": "41250",
                "tx": "127.02547207054795",
                "ty": "37.504757877769535",
                "railLinkList": [
                  {
                    "railLinkId": "1"
                  },
                  {
                    "railLinkId": "2"
                  },
                  {
                    "railLinkId": "3"
                  },
                  {
                    "railLinkId": "4"
                  },
                  {
                    "railLinkId": "5"
                  },
                  {
                    "railLinkId": "6"
                  },
                  {
                    "railLinkId": "7"
                  },
                  {
                    "railLinkId": "8"
                  },
                  {
                    "railLinkId": "9"
                  },
                  {
                    "railLinkId": "10"
                  },
                  {
                    "railLinkId": "11"
                  },
                  {
                    "railLinkId": "12"
                  },
                  {
                    "railLinkId": "13"
                  },
                  {
                    "railLinkId": "14"
                  },
                  {
                    "railLinkId": "15"
                  }
                ]
              },
              {
                "routeNm": "6002-1B화성",
                "routeId": "233000353",
                "fname": "신논현역.영신빌딩",
                "fid": "121000941",
                "fx": "127.02514323951621",
                "fy": "37.50232497252528",
                "tname": "신분당선강남역",
                "tid": "121000009",
                "tx": "127.02839826372518",
                "ty": "37.49598948243004",
                "railLinkList": null
              }
            ]
          },
          {
            "time": "58",
            "distance": "19032",
            "pathList": [
              {
                "routeNm": "5616",
                "routeId": "100100273",
                "fname": "목동시장.대일고교.배광교회",
                "fid": "114000013",
                "fx": "126.86382293571833",
                "fy": "37.538950917807185",
                "tname": "염창역.서울도시가스",
                "tid": "115000002",
                "tx": "126.87498987442348",
                "ty": "37.546910929457056",
                "railLinkList": null
              },
              {
                "routeNm": "9호선",
                "routeId": null,
                "fname": "염창역",
                "fid": "41100",
                "fx": "126.87485402276236",
                "fy": "37.54693781841077",
                "tname": "신논현역",
                "tid": "41250",
                "tx": "127.02547207054795",
                "ty": "37.504757877769535",
                "railLinkList": [
                  {
                    "railLinkId": "1"
                  },
                  {
                    "railLinkId": "2"
                  },
                  {
                    "railLinkId": "3"
                  },
                  {
                    "railLinkId": "4"
                  },
                  {
                    "railLinkId": "5"
                  },
                  {
                    "railLinkId": "6"
                  },
                  {
                    "railLinkId": "7"
                  },
                  {
                    "railLinkId": "8"
                  },
                  {
                    "railLinkId": "9"
                  },
                  {
                    "railLinkId": "10"
                  },
                  {
                    "railLinkId": "11"
                  },
                  {
                    "railLinkId": "12"
                  },
                  {
                    "railLinkId": "13"
                  },
                  {
                    "railLinkId": "14"
                  },
                  {
                    "railLinkId": "15"
                  }
                ]
              },
              {
                "routeNm": "6004광주",
                "routeId": "234000314",
                "fname": "신논현역.영신빌딩",
                "fid": "121000941",
                "fx": "127.02514323951621",
                "fy": "37.50232497252528",
                "tname": "신분당선강남역",
                "tid": "121000009",
                "tx": "127.02839826372518",
                "ty": "37.49598948243004",
                "railLinkList": null
              }
            ]
          },
          {
            "time": "58",
            "distance": "19032",
            "pathList": [
              {
                "routeNm": "6716",
                "routeId": "100100451",
                "fname": "목동시장.대일고교.배광교회",
                "fid": "114000013",
                "fx": "126.86382293571833",
                "fy": "37.538950917807185",
                "tname": "염창역.서울도시가스",
                "tid": "115000002",
                "tx": "126.87498987442348",
                "ty": "37.546910929457056",
                "railLinkList": null
              },
              {
                "routeNm": "9호선",
                "routeId": null,
                "fname": "염창역",
                "fid": "41100",
                "fx": "126.87485402276236",
                "fy": "37.54693781841077",
                "tname": "신논현역",
                "tid": "41250",
                "tx": "127.02547207054795",
                "ty": "37.504757877769535",
                "railLinkList": [
                  {
                    "railLinkId": "1"
                  },
                  {
                    "railLinkId": "2"
                  },
                  {
                    "railLinkId": "3"
                  },
                  {
                    "railLinkId": "4"
                  },
                  {
                    "railLinkId": "5"
                  },
                  {
                    "railLinkId": "6"
                  },
                  {
                    "railLinkId": "7"
                  },
                  {
                    "railLinkId": "8"
                  },
                  {
                    "railLinkId": "9"
                  },
                  {
                    "railLinkId": "10"
                  },
                  {
                    "railLinkId": "11"
                  },
                  {
                    "railLinkId": "12"
                  },
                  {
                    "railLinkId": "13"
                  },
                  {
                    "railLinkId": "14"
                  },
                  {
                    "railLinkId": "15"
                  }
                ]
              },
              {
                "routeNm": "1550-1광주",
                "routeId": "234000324",
                "fname": "신논현역.영신빌딩",
                "fid": "121000941",
                "fx": "127.02514323951621",
                "fy": "37.50232497252528",
                "tname": "신분당선강남역",
                "tid": "121000009",
                "tx": "127.02839826372518",
                "ty": "37.49598948243004",
                "railLinkList": null
              }
            ]
          },
          {
            "time": "58",
            "distance": "19032",
            "pathList": [
              {
                "routeNm": "602",
                "routeId": "100100087",
                "fname": "목동시장.대일고교.배광교회",
                "fid": "114000013",
                "fx": "126.86382293571833",
                "fy": "37.538950917807185",
                "tname": "염창역.서울도시가스",
                "tid": "115000002",
                "tx": "126.87498987442348",
                "ty": "37.546910929457056",
                "railLinkList": null
              },
              {
                "routeNm": "9호선",
                "routeId": null,
                "fname": "염창역",
                "fid": "41100",
                "fx": "126.87485402276236",
                "fy": "37.54693781841077",
                "tname": "신논현역",
                "tid": "41250",
                "tx": "127.02547207054795",
                "ty": "37.504757877769535",
                "railLinkList": [
                  {
                    "railLinkId": "1"
                  },
                  {
                    "railLinkId": "2"
                  },
                  {
                    "railLinkId": "3"
                  },
                  {
                    "railLinkId": "4"
                  },
                  {
                    "railLinkId": "5"
                  },
                  {
                    "railLinkId": "6"
                  },
                  {
                    "railLinkId": "7"
                  },
                  {
                    "railLinkId": "8"
                  },
                  {
                    "railLinkId": "9"
                  },
                  {
                    "railLinkId": "10"
                  },
                  {
                    "railLinkId": "11"
                  },
                  {
                    "railLinkId": "12"
                  },
                  {
                    "railLinkId": "13"
                  },
                  {
                    "railLinkId": "14"
                  },
                  {
                    "railLinkId": "15"
                  }
                ]
              },
              {
                "routeNm": "M4448화성",
                "routeId": "233000332",
                "fname": "신논현역.영신빌딩",
                "fid": "121000941",
                "fx": "127.02514323951621",
                "fy": "37.50232497252528",
                "tname": "신분당선강남역",
                "tid": "121000009",
                "tx": "127.02839826372518",
                "ty": "37.49598948243004",
                "railLinkList": null
              }
            ]
          },
          {
            "time": "58",
            "distance": "19032",
            "pathList": [
              {
                "routeNm": "6714",
                "routeId": "100100331",
                "fname": "목동시장.대일고교.배광교회",
                "fid": "114000013",
                "fx": "126.86382293571833",
                "fy": "37.538950917807185",
                "tname": "염창역.서울도시가스",
                "tid": "115000002",
                "tx": "126.87498987442348",
                "ty": "37.546910929457056",
                "railLinkList": null
              },
              {
                "routeNm": "9호선",
                "routeId": null,
                "fname": "염창역",
                "fid": "41100",
                "fx": "126.87485402276236",
                "fy": "37.54693781841077",
                "tname": "신논현역",
                "tid": "41250",
                "tx": "127.02547207054795",
                "ty": "37.504757877769535",
                "railLinkList": [
                  {
                    "railLinkId": "1"
                  },
                  {
                    "railLinkId": "2"
                  },
                  {
                    "railLinkId": "3"
                  },
                  {
                    "railLinkId": "4"
                  },
                  {
                    "railLinkId": "5"
                  },
                  {
                    "railLinkId": "6"
                  },
                  {
                    "railLinkId": "7"
                  },
                  {
                    "railLinkId": "8"
                  },
                  {
                    "railLinkId": "9"
                  },
                  {
                    "railLinkId": "10"
                  },
                  {
                    "railLinkId": "11"
                  },
                  {
                    "railLinkId": "12"
                  },
                  {
                    "railLinkId": "13"
                  },
                  {
                    "railLinkId": "14"
                  },
                  {
                    "railLinkId": "15"
                  }
                ]
              },
              {
                "routeNm": "M4434광주",
                "routeId": "234001619",
                "fname": "신논현역.영신빌딩",
                "fid": "121000941",
                "fx": "127.02514323951621",
                "fy": "37.50232497252528",
                "tname": "신분당선강남역",
                "tid": "121000009",
                "tx": "127.02839826372518",
                "ty": "37.49598948243004",
                "railLinkList": null
              }
            ]
          },
          {
            "time": "58",
            "distance": "19032",
            "pathList": [
              {
                "routeNm": "6716",
                "routeId": "100100451",
                "fname": "목동시장.대일고교.배광교회",
                "fid": "114000013",
                "fx": "126.86382293571833",
                "fy": "37.538950917807185",
                "tname": "염창역.서울도시가스",
                "tid": "115000002",
                "tx": "126.87498987442348",
                "ty": "37.546910929457056",
                "railLinkList": null
              },
              {
                "routeNm": "9호선",
                "routeId": null,
                "fname": "염창역",
                "fid": "41100",
                "fx": "126.87485402276236",
                "fy": "37.54693781841077",
                "tname": "신논현역",
                "tid": "41250",
                "tx": "127.02547207054795",
                "ty": "37.504757877769535",
                "railLinkList": [
                  {
                    "railLinkId": "1"
                  },
                  {
                    "railLinkId": "2"
                  },
                  {
                    "railLinkId": "3"
                  },
                  {
                    "railLinkId": "4"
                  },
                  {
                    "railLinkId": "5"
                  },
                  {
                    "railLinkId": "6"
                  },
                  {
                    "railLinkId": "7"
                  },
                  {
                    "railLinkId": "8"
                  },
                  {
                    "railLinkId": "9"
                  },
                  {
                    "railLinkId": "10"
                  },
                  {
                    "railLinkId": "11"
                  },
                  {
                    "railLinkId": "12"
                  },
                  {
                    "railLinkId": "13"
                  },
                  {
                    "railLinkId": "14"
                  },
                  {
                    "railLinkId": "15"
                  }
                ]
              },
              {
                "routeNm": "6001화성",
                "routeId": "233000131",
                "fname": "신논현역.영신빌딩",
                "fid": "121000941",
                "fx": "127.02514323951621",
                "fy": "37.50232497252528",
                "tname": "신분당선강남역",
                "tid": "121000009",
                "tx": "127.02839826372518",
                "ty": "37.49598948243004",
                "railLinkList": null
              }
            ]
          },
          {
            "time": "58",
            "distance": "19032",
            "pathList": [
              {
                "routeNm": "6716",
                "routeId": "100100451",
                "fname": "목동시장.대일고교.배광교회",
                "fid": "114000013",
                "fx": "126.86382293571833",
                "fy": "37.538950917807185",
                "tname": "염창역.서울도시가스",
                "tid": "115000002",
                "tx": "126.87498987442348",
                "ty": "37.546910929457056",
                "railLinkList": null
              },
              {
                "routeNm": "9호선",
                "routeId": null,
                "fname": "염창역",
                "fid": "41100",
                "fx": "126.87485402276236",
                "fy": "37.54693781841077",
                "tname": "신논현역",
                "tid": "41250",
                "tx": "127.02547207054795",
                "ty": "37.504757877769535",
                "railLinkList": [
                  {
                    "railLinkId": "1"
                  },
                  {
                    "railLinkId": "2"
                  },
                  {
                    "railLinkId": "3"
                  },
                  {
                    "railLinkId": "4"
                  },
                  {
                    "railLinkId": "5"
                  },
                  {
                    "railLinkId": "6"
                  },
                  {
                    "railLinkId": "7"
                  },
                  {
                    "railLinkId": "8"
                  },
                  {
                    "railLinkId": "9"
                  },
                  {
                    "railLinkId": "10"
                  },
                  {
                    "railLinkId": "11"
                  },
                  {
                    "railLinkId": "12"
                  },
                  {
                    "railLinkId": "13"
                  },
                  {
                    "railLinkId": "14"
                  },
                  {
                    "railLinkId": "15"
                  }
                ]
              },
              {
                "routeNm": "6002-1화성",
                "routeId": "233000142",
                "fname": "신논현역.영신빌딩",
                "fid": "121000941",
                "fx": "127.02514323951621",
                "fy": "37.50232497252528",
                "tname": "신분당선강남역",
                "tid": "121000009",
                "tx": "127.02839826372518",
                "ty": "37.49598948243004",
                "railLinkList": null
              }
            ]
          },
          {
            "time": "58",
            "distance": "19032",
            "pathList": [
              {
                "routeNm": "6716",
                "routeId": "100100451",
                "fname": "목동시장.대일고교.배광교회",
                "fid": "114000013",
                "fx": "126.86382293571833",
                "fy": "37.538950917807185",
                "tname": "염창역.서울도시가스",
                "tid": "115000002",
                "tx": "126.87498987442348",
                "ty": "37.546910929457056",
                "railLinkList": null
              },
              {
                "routeNm": "9호선",
                "routeId": null,
                "fname": "염창역",
                "fid": "41100",
                "fx": "126.87485402276236",
                "fy": "37.54693781841077",
                "tname": "신논현역",
                "tid": "41250",
                "tx": "127.02547207054795",
                "ty": "37.504757877769535",
                "railLinkList": [
                  {
                    "railLinkId": "1"
                  },
                  {
                    "railLinkId": "2"
                  },
                  {
                    "railLinkId": "3"
                  },
                  {
                    "railLinkId": "4"
                  },
                  {
                    "railLinkId": "5"
                  },
                  {
                    "railLinkId": "6"
                  },
                  {
                    "railLinkId": "7"
                  },
                  {
                    "railLinkId": "8"
                  },
                  {
                    "railLinkId": "9"
                  },
                  {
                    "railLinkId": "10"
                  },
                  {
                    "railLinkId": "11"
                  },
                  {
                    "railLinkId": "12"
                  },
                  {
                    "railLinkId": "13"
                  },
                  {
                    "railLinkId": "14"
                  },
                  {
                    "railLinkId": "15"
                  }
                ]
              },
              {
                "routeNm": "6002화성",
                "routeId": "233000136",
                "fname": "신논현역.영신빌딩",
                "fid": "121000941",
                "fx": "127.02514323951621",
                "fy": "37.50232497252528",
                "tname": "신분당선강남역",
                "tid": "121000009",
                "tx": "127.02839826372518",
                "ty": "37.49598948243004",
                "railLinkList": null
              }
            ]
          },
          {
            "time": "58",
            "distance": "19032",
            "pathList": [
              {
                "routeNm": "602",
                "routeId": "100100087",
                "fname": "목동시장.대일고교.배광교회",
                "fid": "114000013",
                "fx": "126.86382293571833",
                "fy": "37.538950917807185",
                "tname": "염창역.서울도시가스",
                "tid": "115000002",
                "tx": "126.87498987442348",
                "ty": "37.546910929457056",
                "railLinkList": null
              },
              {
                "routeNm": "9호선",
                "routeId": null,
                "fname": "염창역",
                "fid": "41100",
                "fx": "126.87485402276236",
                "fy": "37.54693781841077",
                "tname": "신논현역",
                "tid": "41250",
                "tx": "127.02547207054795",
                "ty": "37.504757877769535",
                "railLinkList": [
                  {
                    "railLinkId": "1"
                  },
                  {
                    "railLinkId": "2"
                  },
                  {
                    "railLinkId": "3"
                  },
                  {
                    "railLinkId": "4"
                  },
                  {
                    "railLinkId": "5"
                  },
                  {
                    "railLinkId": "6"
                  },
                  {
                    "railLinkId": "7"
                  },
                  {
                    "railLinkId": "8"
                  },
                  {
                    "railLinkId": "9"
                  },
                  {
                    "railLinkId": "10"
                  },
                  {
                    "railLinkId": "11"
                  },
                  {
                    "railLinkId": "12"
                  },
                  {
                    "railLinkId": "13"
                  },
                  {
                    "railLinkId": "14"
                  },
                  {
                    "railLinkId": "15"
                  }
                ]
              },
              {
                "routeNm": "1552광주",
                "routeId": "234001708",
                "fname": "신논현역.영신빌딩",
                "fid": "121000941",
                "fx": "127.02514323951621",
                "fy": "37.50232497252528",
                "tname": "신분당선강남역",
                "tid": "121000009",
                "tx": "127.02839826372518",
                "ty": "37.49598948243004",
                "railLinkList": null
              }
            ]
          },
          {
            "time": "58",
            "distance": "19032",
            "pathList": [
              {
                "routeNm": "602",
                "routeId": "100100087",
                "fname": "목동시장.대일고교.배광교회",
                "fid": "114000013",
                "fx": "126.86382293571833",
                "fy": "37.538950917807185",
                "tname": "염창역.서울도시가스",
                "tid": "115000002",
                "tx": "126.87498987442348",
                "ty": "37.546910929457056",
                "railLinkList": null
              },
              {
                "routeNm": "9호선",
                "routeId": null,
                "fname": "염창역",
                "fid": "41100",
                "fx": "126.87485402276236",
                "fy": "37.54693781841077",
                "tname": "신논현역",
                "tid": "41250",
                "tx": "127.02547207054795",
                "ty": "37.504757877769535",
                "railLinkList": [
                  {
                    "railLinkId": "1"
                  },
                  {
                    "railLinkId": "2"
                  },
                  {
                    "railLinkId": "3"
                  },
                  {
                    "railLinkId": "4"
                  },
                  {
                    "railLinkId": "5"
                  },
                  {
                    "railLinkId": "6"
                  },
                  {
                    "railLinkId": "7"
                  },
                  {
                    "railLinkId": "8"
                  },
                  {
                    "railLinkId": "9"
                  },
                  {
                    "railLinkId": "10"
                  },
                  {
                    "railLinkId": "11"
                  },
                  {
                    "railLinkId": "12"
                  },
                  {
                    "railLinkId": "13"
                  },
                  {
                    "railLinkId": "14"
                  },
                  {
                    "railLinkId": "15"
                  }
                ]
              },
              {
                "routeNm": "6002-1화성",
                "routeId": "233000142",
                "fname": "신논현역.영신빌딩",
                "fid": "121000941",
                "fx": "127.02514323951621",
                "fy": "37.50232497252528",
                "tname": "신분당선강남역",
                "tid": "121000009",
                "tx": "127.02839826372518",
                "ty": "37.49598948243004",
                "railLinkList": null
              }
            ]
          },
          {
            "time": "58",
            "distance": "19032",
            "pathList": [
              {
                "routeNm": "602",
                "routeId": "100100087",
                "fname": "목동시장.대일고교.배광교회",
                "fid": "114000013",
                "fx": "126.86382293571833",
                "fy": "37.538950917807185",
                "tname": "염창역.서울도시가스",
                "tid": "115000002",
                "tx": "126.87498987442348",
                "ty": "37.546910929457056",
                "railLinkList": null
              },
              {
                "routeNm": "9호선",
                "routeId": null,
                "fname": "염창역",
                "fid": "41100",
                "fx": "126.87485402276236",
                "fy": "37.54693781841077",
                "tname": "신논현역",
                "tid": "41250",
                "tx": "127.02547207054795",
                "ty": "37.504757877769535",
                "railLinkList": [
                  {
                    "railLinkId": "1"
                  },
                  {
                    "railLinkId": "2"
                  },
                  {
                    "railLinkId": "3"
                  },
                  {
                    "railLinkId": "4"
                  },
                  {
                    "railLinkId": "5"
                  },
                  {
                    "railLinkId": "6"
                  },
                  {
                    "railLinkId": "7"
                  },
                  {
                    "railLinkId": "8"
                  },
                  {
                    "railLinkId": "9"
                  },
                  {
                    "railLinkId": "10"
                  },
                  {
                    "railLinkId": "11"
                  },
                  {
                    "railLinkId": "12"
                  },
                  {
                    "railLinkId": "13"
                  },
                  {
                    "railLinkId": "14"
                  },
                  {
                    "railLinkId": "15"
                  }
                ]
              },
              {
                "routeNm": "6002화성",
                "routeId": "233000136",
                "fname": "신논현역.영신빌딩",
                "fid": "121000941",
                "fx": "127.02514323951621",
                "fy": "37.50232497252528",
                "tname": "신분당선강남역",
                "tid": "121000009",
                "tx": "127.02839826372518",
                "ty": "37.49598948243004",
                "railLinkList": null
              }
            ]
          },
          {
            "time": "58",
            "distance": "19032",
            "pathList": [
              {
                "routeNm": "602",
                "routeId": "100100087",
                "fname": "목동시장.대일고교.배광교회",
                "fid": "114000013",
                "fx": "126.86382293571833",
                "fy": "37.538950917807185",
                "tname": "염창역.서울도시가스",
                "tid": "115000002",
                "tx": "126.87498987442348",
                "ty": "37.546910929457056",
                "railLinkList": null
              },
              {
                "routeNm": "9호선",
                "routeId": null,
                "fname": "염창역",
                "fid": "41100",
                "fx": "126.87485402276236",
                "fy": "37.54693781841077",
                "tname": "신논현역",
                "tid": "41250",
                "tx": "127.02547207054795",
                "ty": "37.504757877769535",
                "railLinkList": [
                  {
                    "railLinkId": "1"
                  },
                  {
                    "railLinkId": "2"
                  },
                  {
                    "railLinkId": "3"
                  },
                  {
                    "railLinkId": "4"
                  },
                  {
                    "railLinkId": "5"
                  },
                  {
                    "railLinkId": "6"
                  },
                  {
                    "railLinkId": "7"
                  },
                  {
                    "railLinkId": "8"
                  },
                  {
                    "railLinkId": "9"
                  },
                  {
                    "railLinkId": "10"
                  },
                  {
                    "railLinkId": "11"
                  },
                  {
                    "railLinkId": "12"
                  },
                  {
                    "railLinkId": "13"
                  },
                  {
                    "railLinkId": "14"
                  },
                  {
                    "railLinkId": "15"
                  }
                ]
              },
              {
                "routeNm": "1551광주",
                "routeId": "234001138",
                "fname": "신논현역.영신빌딩",
                "fid": "121000941",
                "fx": "127.02514323951621",
                "fy": "37.50232497252528",
                "tname": "신분당선강남역",
                "tid": "121000009",
                "tx": "127.02839826372518",
                "ty": "37.49598948243004",
                "railLinkList": null
              }
            ]
          },
          {
            "time": "58",
            "distance": "19032",
            "pathList": [
              {
                "routeNm": "6623",
                "routeId": "100100301",
                "fname": "목동시장.대일고교.배광교회",
                "fid": "114000013",
                "fx": "126.86382293571833",
                "fy": "37.538950917807185",
                "tname": "염창역.서울도시가스",
                "tid": "115000002",
                "tx": "126.87498987442348",
                "ty": "37.546910929457056",
                "railLinkList": null
              },
              {
                "routeNm": "9호선",
                "routeId": null,
                "fname": "염창역",
                "fid": "41100",
                "fx": "126.87485402276236",
                "fy": "37.54693781841077",
                "tname": "신논현역",
                "tid": "41250",
                "tx": "127.02547207054795",
                "ty": "37.504757877769535",
                "railLinkList": [
                  {
                    "railLinkId": "1"
                  },
                  {
                    "railLinkId": "2"
                  },
                  {
                    "railLinkId": "3"
                  },
                  {
                    "railLinkId": "4"
                  },
                  {
                    "railLinkId": "5"
                  },
                  {
                    "railLinkId": "6"
                  },
                  {
                    "railLinkId": "7"
                  },
                  {
                    "railLinkId": "8"
                  },
                  {
                    "railLinkId": "9"
                  },
                  {
                    "railLinkId": "10"
                  },
                  {
                    "railLinkId": "11"
                  },
                  {
                    "railLinkId": "12"
                  },
                  {
                    "railLinkId": "13"
                  },
                  {
                    "railLinkId": "14"
                  },
                  {
                    "railLinkId": "15"
                  }
                ]
              },
              {
                "routeNm": "M4448화성",
                "routeId": "233000332",
                "fname": "신논현역.영신빌딩",
                "fid": "121000941",
                "fx": "127.02514323951621",
                "fy": "37.50232497252528",
                "tname": "신분당선강남역",
                "tid": "121000009",
                "tx": "127.02839826372518",
                "ty": "37.49598948243004",
                "railLinkList": null
              }
            ]
          },
          {
            "time": "58",
            "distance": "19032",
            "pathList": [
              {
                "routeNm": "602",
                "routeId": "100100087",
                "fname": "목동시장.대일고교.배광교회",
                "fid": "114000013",
                "fx": "126.86382293571833",
                "fy": "37.538950917807185",
                "tname": "염창역.서울도시가스",
                "tid": "115000002",
                "tx": "126.87498987442348",
                "ty": "37.546910929457056",
                "railLinkList": null
              },
              {
                "routeNm": "9호선",
                "routeId": null,
                "fname": "염창역",
                "fid": "41100",
                "fx": "126.87485402276236",
                "fy": "37.54693781841077",
                "tname": "신논현역",
                "tid": "41250",
                "tx": "127.02547207054795",
                "ty": "37.504757877769535",
                "railLinkList": [
                  {
                    "railLinkId": "1"
                  },
                  {
                    "railLinkId": "2"
                  },
                  {
                    "railLinkId": "3"
                  },
                  {
                    "railLinkId": "4"
                  },
                  {
                    "railLinkId": "5"
                  },
                  {
                    "railLinkId": "6"
                  },
                  {
                    "railLinkId": "7"
                  },
                  {
                    "railLinkId": "8"
                  },
                  {
                    "railLinkId": "9"
                  },
                  {
                    "railLinkId": "10"
                  },
                  {
                    "railLinkId": "11"
                  },
                  {
                    "railLinkId": "12"
                  },
                  {
                    "railLinkId": "13"
                  },
                  {
                    "railLinkId": "14"
                  },
                  {
                    "railLinkId": "15"
                  }
                ]
              },
              {
                "routeNm": "6001화성",
                "routeId": "233000131",
                "fname": "신논현역.영신빌딩",
                "fid": "121000941",
                "fx": "127.02514323951621",
                "fy": "37.50232497252528",
                "tname": "신분당선강남역",
                "tid": "121000009",
                "tx": "127.02839826372518",
                "ty": "37.49598948243004",
                "railLinkList": null
              }
            ]
          },
          {
            "time": "58",
            "distance": "19032",
            "pathList": [
              {
                "routeNm": "6623",
                "routeId": "100100301",
                "fname": "목동시장.대일고교.배광교회",
                "fid": "114000013",
                "fx": "126.86382293571833",
                "fy": "37.538950917807185",
                "tname": "염창역.서울도시가스",
                "tid": "115000002",
                "tx": "126.87498987442348",
                "ty": "37.546910929457056",
                "railLinkList": null
              },
              {
                "routeNm": "9호선",
                "routeId": null,
                "fname": "염창역",
                "fid": "41100",
                "fx": "126.87485402276236",
                "fy": "37.54693781841077",
                "tname": "신논현역",
                "tid": "41250",
                "tx": "127.02547207054795",
                "ty": "37.504757877769535",
                "railLinkList": [
                  {
                    "railLinkId": "1"
                  },
                  {
                    "railLinkId": "2"
                  },
                  {
                    "railLinkId": "3"
                  },
                  {
                    "railLinkId": "4"
                  },
                  {
                    "railLinkId": "5"
                  },
                  {
                    "railLinkId": "6"
                  },
                  {
                    "railLinkId": "7"
                  },
                  {
                    "railLinkId": "8"
                  },
                  {
                    "railLinkId": "9"
                  },
                  {
                    "railLinkId": "10"
                  },
                  {
                    "railLinkId": "11"
                  },
                  {
                    "railLinkId": "12"
                  },
                  {
                    "railLinkId": "13"
                  },
                  {
                    "railLinkId": "14"
                  },
                  {
                    "railLinkId": "15"
                  }
                ]
              },
              {
                "routeNm": "8502화성",
                "routeId": "233000333",
                "fname": "신논현역.영신빌딩",
                "fid": "121000941",
                "fx": "127.02514323951621",
                "fy": "37.50232497252528",
                "tname": "신분당선강남역",
                "tid": "121000009",
                "tx": "127.02839826372518",
                "ty": "37.49598948243004",
                "railLinkList": null
              }
            ]
          },
          {
            "time": "58",
            "distance": "19032",
            "pathList": [
              {
                "routeNm": "6623",
                "routeId": "100100301",
                "fname": "목동시장.대일고교.배광교회",
                "fid": "114000013",
                "fx": "126.86382293571833",
                "fy": "37.538950917807185",
                "tname": "염창역.서울도시가스",
                "tid": "115000002",
                "tx": "126.87498987442348",
                "ty": "37.546910929457056",
                "railLinkList": null
              },
              {
                "routeNm": "9호선",
                "routeId": null,
                "fname": "염창역",
                "fid": "41100",
                "fx": "126.87485402276236",
                "fy": "37.54693781841077",
                "tname": "신논현역",
                "tid": "41250",
                "tx": "127.02547207054795",
                "ty": "37.504757877769535",
                "railLinkList": [
                  {
                    "railLinkId": "1"
                  },
                  {
                    "railLinkId": "2"
                  },
                  {
                    "railLinkId": "3"
                  },
                  {
                    "railLinkId": "4"
                  },
                  {
                    "railLinkId": "5"
                  },
                  {
                    "railLinkId": "6"
                  },
                  {
                    "railLinkId": "7"
                  },
                  {
                    "railLinkId": "8"
                  },
                  {
                    "railLinkId": "9"
                  },
                  {
                    "railLinkId": "10"
                  },
                  {
                    "railLinkId": "11"
                  },
                  {
                    "railLinkId": "12"
                  },
                  {
                    "railLinkId": "13"
                  },
                  {
                    "railLinkId": "14"
                  },
                  {
                    "railLinkId": "15"
                  }
                ]
              },
              {
                "routeNm": "6002화성",
                "routeId": "233000136",
                "fname": "신논현역.영신빌딩",
                "fid": "121000941",
                "fx": "127.02514323951621",
                "fy": "37.50232497252528",
                "tname": "신분당선강남역",
                "tid": "121000009",
                "tx": "127.02839826372518",
                "ty": "37.49598948243004",
                "railLinkList": null
              }
            ]
          }
        ]
      }
    };

    return PathInfoResponse
        .fromJson(resultJson)
        .msgBody;
  }
}