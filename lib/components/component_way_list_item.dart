import 'package:app_public_path_way/model/path_info_item.dart';
import 'package:flutter/material.dart';

class ComponentWayListItem extends StatelessWidget {
  const ComponentWayListItem({super.key, required this.item, required this.callback});

  final PathInfoItem item;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        margin: const EdgeInsets.fromLTRB(20, 5, 20, 5),
        decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(
              color: const Color.fromRGBO(50, 50, 50, 100),
              width: 3
          ),
          borderRadius: BorderRadius.circular(10),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Container(
              padding: const EdgeInsets.all(10),
              child: Column(
                children: [
                  for (int i = 0; i < item.pathList.length; i++)
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Row(
                          children: [
                            Icon(
                              item.pathList[i].isSubway
                                  ? Icons.subway
                                  : Icons.bus_alert,
                              size: 20,
                            ),
                            Text(
                              ' ${item.pathList[i].routeNm}',
                              style: const TextStyle(
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                            const Text(':'),
                          ],
                        ),
                        Text('${item.pathList[i].fname} -> ${item.pathList[i].tname}'),
                        if (i != item.pathList.length - 1) const Divider(),
                      ],
                    ),
                ],
              ),
            ),
            Container(
              padding: const EdgeInsets.all(10),
              decoration: const BoxDecoration(
                border: Border(
                  top: BorderSide(
                    color: Color.fromRGBO(10, 10, 10, 100),
                    width: 3,
                  ),
                ),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Row(
                    children: [
                      const Text(
                        '총 소요시간',
                        style: TextStyle(
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      Text(': ${item.time}분')
                    ],
                  ),
                  Row(
                    children: [
                      const Text(
                        '거리',
                        style: TextStyle(
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      Text(': ${item.distance}km')
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
