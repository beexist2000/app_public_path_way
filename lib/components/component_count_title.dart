import 'package:flutter/material.dart';

class ComponentCountTitle extends StatelessWidget {
  const ComponentCountTitle({
    super.key,
    required this.icon,
    required this.count,
    required this.unitName,
    required this.itemName
  });

  final IconData icon;
  final int count;
  final String unitName;
  final String itemName;


  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          Icon(icon, size: 25,),
          const SizedBox(width: 5,),
          Text('총 ${count.toString()}$unitName의 $itemName을(를) 찾았습니다.'),
        ],
      ),
    );
  }
}
