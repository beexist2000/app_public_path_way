import 'package:flutter/material.dart';

class ComponentAppbarFilter extends StatefulWidget implements PreferredSizeWidget {
  const ComponentAppbarFilter({super.key, required this.title, required this.isUseActionButton, required this.icon, required this.callback});

  final String title;
  final bool isUseActionButton;
  final IconData icon;
  final VoidCallback callback;

  @override
  State<ComponentAppbarFilter> createState() => _ComponentAppbarFilterState();

  @override
  // TODO: implement preferredSize
  Size get preferredSize => const Size.fromHeight(40);
}

class _ComponentAppbarFilterState extends State<ComponentAppbarFilter> {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      centerTitle: false,
      automaticallyImplyLeading: false,
      title: Text(
        widget.title,
        style: const TextStyle(
          fontSize: 14,
        ),
      ),
      elevation: 1.0,
      actions: [
        widget.isUseActionButton ? IconButton(onPressed: widget.callback, icon: Icon(widget.icon)) : const Center(),
      ],
    );
  }
}
