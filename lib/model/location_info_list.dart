import 'package:app_public_path_way/model/location_info_item.dart';

class LocationInfoList {
  List<LocationInfoItem> itemList;

  LocationInfoList(this.itemList);

  factory LocationInfoList.fromJson(Map<String, dynamic> json) {
    return LocationInfoList(
        json['itemList'] == null ? [] : (json['itemList'] as List).map((e) => LocationInfoItem.fromJson(e)).toList()
    );
  }
}