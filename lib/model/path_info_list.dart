import 'package:app_public_path_way/model/path_info_item.dart';

class PathInfoList {
  List<PathInfoItem> itemList;

  PathInfoList(this.itemList);

  factory PathInfoList.fromJson(Map<String, dynamic> json) {
    return PathInfoList(
      json['itemList'] == null ? [] : (json['itemList'] as List).map((e) => PathInfoItem.fromJson(e)).toList(),
    );
  }
}