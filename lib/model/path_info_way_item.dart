import 'package:app_public_path_way/model/path_info_way_rail_item.dart';

class PathInfoWayItem {
  String routeNm;
  String fname;
  String tname;
  bool isSubway;

  PathInfoWayItem(
      this.routeNm,
      this.fname,
      this.tname,
      this.isSubway);
  
  factory PathInfoWayItem.fromJson(Map<String, dynamic> json) {
    // List<PathInfoWayRailItem> railLinkList = json['railLinkList'] == null 
    //     ? []
    //     : (json['railLinkList'] as List).map((e) => PathInfoWayRailItem(e)).toList();
    //
    // bool isSubway = false;
    // if(railLinkList.isNotEmpty) isSubway = true;
    


    return PathInfoWayItem(
      json['routeNm'],
      json['fname'],
      json['tname'],
      !(json['railLinkList'] == null),
    );
  }
}