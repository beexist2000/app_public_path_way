import 'package:app_public_path_way/model/path_info_list.dart';

class PathInfoResponse {
  PathInfoList msgBody;

  PathInfoResponse(this.msgBody);

  factory PathInfoResponse.fromJson(Map<String, dynamic> json) {
    return PathInfoResponse(
      PathInfoList.fromJson(json['msgBody']),
    );
  }
}