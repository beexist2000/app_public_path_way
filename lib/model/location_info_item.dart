class LocationInfoItem {
  int poiId;
  String poiNm;
  num gpsX;
  num gpsY;

  LocationInfoItem(
      this.poiId,
      this.poiNm,
      this.gpsX,
      this.gpsY);

  factory LocationInfoItem.fromJson(Map<String, dynamic> json) {
    return LocationInfoItem(
        int.parse(json['poiId']),
        json['poiNm'],
        num.parse(json['gpsX']),
        num.parse(json['gpsY']),
    );
  }
}