class PathInfoWayRailItem {
  String railLinkId;

  PathInfoWayRailItem(this.railLinkId);

  factory PathInfoWayRailItem.fromJson(Map<String, dynamic> json) {
    return PathInfoWayRailItem(
      json['railLinkId']
    );
  }
}