import 'package:app_public_path_way/model/path_info_way_item.dart';

class PathInfoItem {
  int time;
  num distance;
  List<PathInfoWayItem> pathList;
  
  PathInfoItem(
      this.time,
      this.distance,
      this.pathList);
  
  factory PathInfoItem.fromJson(Map<String, dynamic> json) {
    return PathInfoItem(
      json['time'] == null ? 0 : int.parse(json['time']),
      num.parse(json['distance']) / 1000,
      json['pathList'] == null ? [] : (json['pathList'] as List).map((e) => PathInfoWayItem.fromJson(e)).toList(),
    );
  }
}