import 'package:app_public_path_way/components/component_appbar_filter.dart';
import 'package:app_public_path_way/components/component_count_title.dart';
import 'package:app_public_path_way/components/component_custom_loading.dart';
import 'package:app_public_path_way/components/component_no_contents.dart';
import 'package:app_public_path_way/components/component_way_list_item.dart';
import 'package:app_public_path_way/model/path_info_item.dart';
import 'package:app_public_path_way/model/path_info_request.dart';
import 'package:app_public_path_way/pages/page_search_location.dart';
import 'package:app_public_path_way/repository/repo_path_info.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';

class PageIndex extends StatefulWidget {
  const PageIndex({Key? key}) : super(key: key);

  @override
  State<PageIndex> createState() => _PageIndexState();
}

class _PageIndexState extends State<PageIndex> with AutomaticKeepAliveClientMixin {
  final _scrollController = ScrollController();

  String startAreaName = '';
  num startAreaX = 0;
  num startAreaY = 0;
  String endAreaName = '';
  num endAreaX = 0;
  num endAreaY = 0;

  List<PathInfoItem> _itemList = [];

  Future<void> _loadItems() async {
    if(startAreaX != 0 && startAreaY != 0 && endAreaX != 0 && endAreaY != 0) {
      BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
        return ComponentCustomLoading(cancelFunc: cancelFunc);
      });

      PathInfoRequest request = PathInfoRequest(startAreaX, startAreaY, endAreaX, endAreaY);

      await RepoPathInfo()
          .getList(request)
          .then((res) {
        BotToast.closeAllLoading();
        setState(() {
          _itemList = res.itemList;
        });
      }).catchError((err) => BotToast.closeAllLoading());

      _scrollController.animateTo(0, duration: const Duration(milliseconds: 300), curve: Curves.easeOut);
    } else {
      BotToast.showSimpleNotification(
          title: "출발지와 도착지는 필수입니다",
          subTitle: "출발지와 도착지를 먼저 전부 검색해주세요",
          enableSlideOff: true,
          hideCloseButton: false,
          backgroundColor: Colors.white,
          titleStyle: const TextStyle(color: Colors.blue),
          subTitleStyle: const TextStyle(color: Colors.grey),
          onlyOne: true,
          crossPage: true,
          backButtonBehavior: BackButtonBehavior.none,
          animationDuration: const Duration(milliseconds: 200),
          animationReverseDuration: const Duration(milliseconds: 200),
          duration: const Duration(seconds: 2));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarFilter(
        title: '서울교통 길찾기',
        isUseActionButton: false,
        icon: Icons.search,
        callback: () {},
      ),
      body: ListView(
        controller: _scrollController,
        children: [
          _buildForm(),
          const Divider(),
          _buildList(),
        ],
      ),
    );
  }

  Widget _buildForm() {
    return Container(
      padding: const EdgeInsets.all(5),
      child: Column(
        children: [
          Container(
            alignment: Alignment.centerLeft,
            child: const Text('출발지'),
          ),
          Row(
            children: [
              Container(
                padding: const EdgeInsets.all(10),
                width: MediaQuery.of(context).size.width - 40 - 5 - 20,
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.grey),
                  color: Colors.white
                ),
                child: Text(startAreaName.isNotEmpty ? startAreaName : '검색 버튼을 눌러주세요.'),
              ),
              const SizedBox(width: 5,),
              Container(
                color: Colors.blue,
                child: IconButton(
                  color: Colors.white,
                  onPressed: () async {
                    final popupResult = await Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                const PageSearchLocation(isStart: true)));

                    if (popupResult != null && popupResult[0] && popupResult[1]) {
                      setState(() {
                        startAreaName = popupResult[2];
                        startAreaX = popupResult[3];
                        startAreaY = popupResult[4];
                      });
                    }
                  },
                  icon: const Icon(Icons.search,),
                ),
              ),
            ],
          ),
          const SizedBox(height: 5,),
          Container(
            alignment: Alignment.centerLeft,
            child: const Text('도착지'),
          ),
          Row(
            children: [
              Container(
                padding: const EdgeInsets.all(10),
                width: MediaQuery.of(context).size.width - 40 - 5 - 20,
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.grey),
                  color: Colors.white
                ),
                child: Text(endAreaName.isNotEmpty ? endAreaName : '검색 버튼을 눌러주세요.'),
              ),
              const SizedBox(width: 5,),
              Container(
                color: Colors.blue,
                child: IconButton(
                  color: Colors.white,
                  onPressed: () async {
                    final popupResult = await Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => const PageSearchLocation(isStart: false))
                    );

                    if (popupResult != null && popupResult[0] && !popupResult[1]) {
                      setState(() {
                        endAreaName = popupResult[2];
                        endAreaX = popupResult[3];
                        endAreaY = popupResult[4];
                      });
                    }
                  },
                  icon: const Icon(Icons.search),
                ),
              ),
            ],
          ),
          const SizedBox(height: 25,),
          OutlinedButton(
            onPressed: () {
              _loadItems();
            },
            child: const Text('조회'),
          ),
        ],
      ),
    );
  }

  Widget _buildList() {
    if(_itemList.isNotEmpty) {
      return Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          ComponentCountTitle(
            icon: Icons.add,
            count: _itemList.length,
            unitName: '건',
            itemName: '경로',
          ),
          const SizedBox(height: 10,),
          ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: _itemList.length,
            itemBuilder: (_, index) => ComponentWayListItem(item: _itemList[index], callback: () {},),
          ),
        ],
      );
    } else {
      return SizedBox(
        height: MediaQuery.of(context).size.height - 40,
        child: const ComponentNoContents(icon: Icons.cancel_outlined, msg: '경로가 없습니다.'),
      );
    }
  }

  @override
  bool get wantKeepAlive => true;
}
