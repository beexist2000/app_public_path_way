import 'package:app_public_path_way/components/component_appbar_popup.dart';
import 'package:app_public_path_way/components/component_custom_loading.dart';
import 'package:app_public_path_way/model/location_info_item.dart';
import 'package:app_public_path_way/repository/repo_location_info.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class PageSearchLocation extends StatefulWidget {
  const PageSearchLocation({super.key, required this.isStart});

  final bool isStart;

  @override
  State<PageSearchLocation> createState() => _PageSearchLocationState();
}

class _PageSearchLocationState extends State<PageSearchLocation> {
  final _formKey = GlobalKey<FormBuilderState>();

  List<LocationInfoItem> _list = [];

  Future<void> _loadItems(String searchKeyword) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoLocationInfo()
        .getList(searchKeyword)
        .then((res) {
      BotToast.closeAllLoading();
          setState(() {
            _list = res.itemList;
          });
    }).catchError((err) => BotToast.closeAllLoading());

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarPopup(title: '${widget.isStart ? '출발지' : '도착지'} 검색'),
      body: _buildBody(),
    );
  }

  Widget _buildBody() {
    return ListView(
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 20, right: 20),
          child: FormBuilder(
            key: _formKey,
            autovalidateMode: AutovalidateMode.disabled,
            child: FormBuilderTextField(
              name: 'searchKeyword',
              decoration: const InputDecoration(
                labelText: '역 또는 정류장 이름',
              ),
              validator: FormBuilderValidators.compose([
                FormBuilderValidators.required(errorText: '검색 키워드를 넣어주세요'),
              ]),
            ),
          ),
        ),
        const SizedBox(height: 10,),
        Container(
          padding: const EdgeInsets.only(left: 100, right: 100),
          child: CupertinoButton(
            color: Colors.blue,
            onPressed: () {
              if (_formKey.currentState?.saveAndValidate() ?? false) {
                String searchKeyword = _formKey.currentState!.fields['searchKeyword']!.value;
                _loadItems(searchKeyword);
              }
            },
            child: const Text('검색'),
          ),
        ),
        ListView.builder(
          physics: const NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          itemCount: _list.length,
          itemBuilder: (_, index) => Container(
            padding: const EdgeInsets.all(5),
            margin: const EdgeInsets.all(5),
            decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(
                color: const Color.fromRGBO(50, 50, 50, 100),
                width: 3,
              ),
            ),
            child: Row(
              children: [
                SizedBox(
                  width: MediaQuery.of(context).size.width - 55 - 10 - 10,
                  child: Column(
                    children: [
                      Text('${_list[index].poiNm} '),
                      Text('(${_list[index].gpsX}, ${_list[index].gpsY})'),
                    ],
                  ),
                ),
                IconButton(
                  alignment: Alignment.centerRight,
                  onPressed: () {
                    Navigator.pop(
                      context,
                      [true, widget.isStart, _list[index].poiNm, _list[index].gpsX, _list[index].gpsY],
                    );
                  },
                  icon: const Icon(Icons.check_box_outlined),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
